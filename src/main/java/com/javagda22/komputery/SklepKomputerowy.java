package com.javagda22.komputery;

import java.util.List;
import java.util.stream.Collectors;

public class SklepKomputerowy {
    private List<OfertaSprzedaży> oferty;
    private List<Komputer> list;

    public SklepKomputerowy(List<Komputer> list, List<OfertaSprzedaży> listaOfert) {
        this.oferty = listaOfert;
        this.list = list;
    }

    public void wypiszWszystkieKomputery() {
//        list.stream()...
        // old
        System.out.println(list);
        // stream
        list.forEach(komputer -> System.out.println(komputer));
    }

    public void wypiszKomputeryOMocyWyzszejNiz(int moc) {
        list.stream()
                .filter(komputer -> komputer.getPobieranaMoc() > moc)
                .forEach(komputer -> System.out.println(komputer)); // WYPISZ!
    }

    public void wypiszCenyNazwyIdentyfikatoryWszystkichKomputerow() {
        list.stream()
                .forEach(komputer ->
                        System.out.println(komputer.getIdentyfikatorProduktu() + ". " + komputer.getNazwa() + " - " + komputer.getCena())
                );
    }

    public List<Komputer> zwrocWszystkieKomputery() {
        return list;
    }

    public void wypiszWszystkieKomputeryZDwomaProcesorami() { // WYPISZ WIĘC NIC NIE ZWRACAMY
        list.stream()
                .filter(komputer -> komputer.getIlośćProcesorów() == 2)
                .forEach(System.out::println);
    }

    public List<Komputer> zwrocWszystkieKomputeryZDwomaProcesorami() { // ZWRÓĆ
        return list.stream()
                .filter(komputer -> komputer.getIlośćProcesorów() == 2)
                .collect(Collectors.toList());
    }

    public double zwrocSredniaIloscProceosorow() {
        double wynik = list.stream()
                .mapToInt(komputer -> komputer.getIlośćProcesorów())
                .average()
                .getAsDouble();
        return wynik;
    }

    public List<Komputer> zwróćTylkoKomputeryZTurboIProcesoremPowyzej4() {
        return list.stream()
                .filter(komputer -> komputer.isCzyProcesorMaTurbo())
                .filter(komputer -> komputer.getIlośćProcesorów() > 4)
                .collect(Collectors.toList());
    }

    public List<OfertaSprzedaży> zwrocWszystkie() {
//        return oferty.stream().
        return oferty;
    }
}
