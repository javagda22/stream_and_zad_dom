package com.javagda22.data_czas;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Scanner;

public class MainParseDate {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        DateTimeFormatter formatter =
                DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");

        String data = scanner.nextLine();

        try {
            LocalDateTime dataCzas =
                    LocalDateTime.parse(data, formatter);


            System.out.println(dataCzas);
        }catch (DateTimeParseException dtpe){
            System.out.println(dtpe.getMessage());
        }


    }
}
