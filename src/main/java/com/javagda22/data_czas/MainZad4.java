package com.javagda22.data_czas;

import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class MainZad4 {
    public static void main(String[] args) {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

        Scanner scanner = new Scanner(System.in);

        // wczytanie i parsowanie 1 daty
        String dataPoczatek = scanner.next();
        LocalDate dataStart = LocalDate.parse(dataPoczatek, dateTimeFormatter);

        // wczytanie i parsowanie 2 daty
        String dataKoniec = scanner.next();
        LocalDate dataStop = LocalDate.parse(dataKoniec, dateTimeFormatter);

        // obliczanie różnicy
        Period period = Period.between(dataStart, dataStop);

        // wypisanie różnicy
        System.out.println("Years: " + period.getYears() + " months: " + period.getMonths() + " days: " + period.getDays());

    }
}
