package com.javagda22.data_czas;


import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Scanner;

public class Main_demo {
    public static void main(String[] args) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy * MM * dd  HH:mm:ss"); // < - format

        Scanner scanner = new Scanner(System.in);
        String polecenie;
        do{
            System.out.println("Wpisz komende: ");
            polecenie = scanner.next();

            if(polecenie.equals("date")){
                System.out.println(LocalDate.now());
            }else if(polecenie.equals("time")){
                System.out.println(LocalTime.now());
            }else if(polecenie.equals("datetime")){
                // używam formatter'a do konwersji czasu na tekst w wybranym formacie
                String czasSformatowany = LocalDateTime.now().format(formatter);

                // wypisuję
                System.out.println(czasSformatowany);
            }
        }while (!polecenie.equals("quit"));
    }
}
