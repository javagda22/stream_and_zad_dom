package com.javagda22.data_czas;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;

public class Main2 {
    public static void main(String[] args) {
        // rozwiązanie zadania 2
        LocalDate teraz = LocalDate.now();

        System.out.println(teraz.minusDays(10));
        System.out.println(teraz.plusDays(10));

        // okres - dni, miesiące, lata
        Period okres = Period.between(
                LocalDate.now().minusDays(10),
                LocalDate.now());

        // okres - dni, miesiące, lata
        Period okres2 = Period.between(
                LocalDate.of(2014, 3, 20),
                LocalDate.of(2018, 5, 10));

        System.out.println(okres.getDays());

        // precyzyjne, dni minuty, sekundy itd.
        Duration duration = Duration.between(
                LocalDateTime.now().minusYears(10),
                LocalDateTime.now());

        System.out.println(duration.getSeconds());


    }
}
