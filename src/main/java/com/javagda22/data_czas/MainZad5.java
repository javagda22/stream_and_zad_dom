package com.javagda22.data_czas;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;

public class MainZad5 {
    public static void main(String[] args) {
        LocalDate dateOfBirth = LocalDate.of(1993, 12, 12);

        Period period = Period.between(dateOfBirth, LocalDate.now());

        System.out.println("Masz: " + period.getYears() + " lat.");
    }
}
