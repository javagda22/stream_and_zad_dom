package com.javagda22.domowe_stream;

import java.util.*;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        Person person1 = new Person("Jacek", "Kowalski", 18, true);
        Person person2 = new Person("Jacek", "Górski", 15, true);
        Person person3 = new Person("Andżelika", "Dżoli", 25, false);
        Person person4 = new Person("Wanda", "Ibanda", 12, false);
        Person person5 = new Person("Marek", "Marecki", 17, true);
        Person person6 = new Person("Johny", "Brawo", 25, true);
        Person person7 = new Person("Stary", "Pan", 80, true);
        Person person8 = new Person("Newbie", "Noob", 12, true);
        Person person9 = new Person("Newbies", "Sister", 19, false);

        List<Person> list = new ArrayList<Person>(Arrays.asList(
                person1,
                person2,
                person3,
                person4,
                person5,
                person6,
                person7,
                person8,
                person9
        ));

//        1.Napisz klasę person, która ma pola: firstName, lastName, age oraz isMale. Mając listę osób i korzystając ze streamów:
//              a) uzyskaj listę mężczyzn
        List<Person> listaMezczyzn = list.stream()
                .filter(person -> person.isMale())
                .collect(Collectors.toList());

        System.out.println(listaMezczyzn);

//              b) uzyskaj listę dorosłych kobiet ​(filter)
        List<Person> listaDoroslychKobiet = list.stream()
                .filter(person -> !person.isMale())
                .filter(person -> person.getAge() > 18)
                .collect(Collectors.toList());

        System.out.println(listaDoroslychKobiet);
//              c) uzyskaj Optional<Person> z dorosłym Jackiem ​findAny/findfirst
        Optional<Person> opcjonalnyJacek = list.stream()
                .filter(person -> person.getFirstName().equals("Jacek") && person.getAge() >= 18)
                .findFirst();

        if (opcjonalnyJacek.isPresent()) {
            Person jacek = opcjonalnyJacek.get(); // wydobywamy z wnętrza optionala
            System.out.println("Znalezlismy doroslego Jacka: " + opcjonalnyJacek); // toString na person jacek
            System.out.println("Znalezlismy doroslego Jacka: " + jacek); // toString na person jacek
        } else {
            System.out.println("Wszystkie Jacki są niedojrzałe!");
        }

//              d) uzyskaj listę wszystkich nazwisk osób, które są w przedziale wiekowym: 15-19 ​(filter)
        list.stream()
                .filter(person -> person.getAge() > 15 && person.getAge() < 19)
                .forEach(System.out::println); // wypisanie nazwisk

        // będą duplikaty
        List<String> nazwiska = list.stream()
                .filter(person -> person.getAge() > 15 && person.getAge() < 19)
                .map(person -> person.getLastName())
                // przez map przechodzą tylko nazwiska
                .collect(Collectors.toList());


        // nie ma duplikatów
        Set<String> nazwiskaSet = list.stream()
                .filter(person -> person.getAge() > 15 && person.getAge() < 19)
                .map(person -> person.getLastName())
                // przez map przechodzą tylko nazwiska
                .collect(Collectors.toSet());

        Set<String> wynik_nazwiska = new HashSet<>();
        for (Person person : list) {
            if (person.getAge() > 15 && person.getAge() < 19) {
                wynik_nazwiska.add(person.getLastName());
            }
        }

//              e)* uzyskaj sumę wieku wszystkich osób ​(sum)
        int sum = list.stream()
                .mapToInt(Person::getAge)
                .sum();

//              f)* uzyskaj średnią wieku wszystkich mężczyzn ​(average)
        OptionalDouble average = list.stream()
                .filter(person -> person.isMale())
                .mapToInt(Person::getAge)
                .average();

        if (average.isPresent()) {
            System.out.println("Średnia: " + average.getAsDouble());
        }

//              g)** znajdź nastarszą osobę w liście ​(findFirst)
        OptionalInt maxAge = list.stream()
                .mapToInt(person -> person.getAge())
                .max();

        if (maxAge.isPresent()) {
            // jeśli udało się znaleźć osobę najstarszą
            Integer wiekNajstarszejOsoby = maxAge.getAsInt();

            Optional<Person> najstarszy = list.stream()
                    .filter(person -> person.getAge() == wiekNajstarszejOsoby)
                    .findAny();
        }

        // to co wyżej zrobione w jednym streamie!
        Optional<Person> najstarszy = list.stream()
                .filter(person -> {
                    return person.getAge() == list.stream()
                            .mapToInt(personI -> personI.getAge())
                            .max().getAsInt();
                }).findAny();


        List<String> languages1 = Arrays.asList("Java;Cobol;Cpp;Lisp".split(";"));
        List<String> languages2 = Arrays.asList("Java;Lisp".split(";"));
        List<String> languages3 = Arrays.asList("Java;Cobol;Cpp;Lisp;C#".split(";"));
        List<String> languages4 = Arrays.asList("C#;C;Cpp".split(";"));
        List<String> languages5 = Arrays.asList("Java;Assembler;Scala;Cobol".split(";"));
        List<String> languages6 = Arrays.asList("Java;Scala".split(";"));
        List<String> languages7 = Arrays.asList("C#;C".split(";"));
        List<String> languages8 = Collections.emptyList();
        List<String> languages9 = Arrays.asList("Java");
        Programmer programmer1 = new Programmer(person1, languages1);
        Programmer programmer2 = new Programmer(person2, languages2);
        Programmer programmer3 = new Programmer(person3, languages3);
        Programmer programmer4 = new Programmer(person4, languages4);
        Programmer programmer5 = new Programmer(person5, languages5);
        Programmer programmer6 = new Programmer(person6, languages6);
        Programmer programmer7 = new Programmer(person7, languages7);
        Programmer programmer8 = new Programmer(person8, languages8);
        Programmer programmer9 = new Programmer(person9, languages9);
        List<Programmer> programmers = Arrays.asList(programmer1, programmer2, programmer3, programmer4, programmer5, programmer6, programmer7, programmer8, programmer9);
        System.out.println(programmers);

//        a) uzyskaj listę programistów, którzy są mężczyznami
        List<Programmer> programFacet = programmers.stream()
                .filter(programmer -> programmer.getPerson().isMale())
                .collect(Collectors.toList());

// b) uzyskaj listę niepełnoletnich programistów (obydwóch płci), którzy piszą w Cobolu
        List<Programmer> niepelnoletniProgramisci = programmers.stream()
                .filter(programmer -> programmer.getPerson().getAge() < 18)
                .filter(programmer -> programmer.getLanguages().contains("Cobol"))
                .collect(Collectors.toList());

// c) uzyskaj listę programistów, którzy znają więcej, niż jeden język programowania
        List<Programmer> kumaci = programmers.stream()
                .filter(programmer -> programmer.getLanguages().size() > 1)
                .collect(Collectors.toList());

// d) uzyskaj listę programistek, które piszą w Javie i Cpp
        List<Programmer> programistki = programmers.stream()
                .filter(programmer -> !programmer.getPerson().isMale())
//                .filter(programmer -> programmer.getLanguages().containsAll(Arrays.asList("Java", "Cpp")))
                .filter(programmer -> programmer.getLanguages().contains("Java") && programmer.getLanguages().contains("Cpp"))
                .collect(Collectors.toList());

// e) uzyskaj listę męskich imion
        List<String> names = programmers.stream()
                .filter(programmer -> programmer.getPerson().isMale())
                .map(programmer -> programmer.getPerson().getFirstName())
                .collect(Collectors.toList());
// f) uzyskaj set wszystkich języków opanowanych przez programistów
        Set<String> jezyki = programmers.stream()
                .map(programmer -> programmer.getLanguages())
                // List<List<String>>
                .flatMap(strings -> strings.stream())
                // zamieniamy Listę list, na jedną listę
                // łączymy wszystkie listy ?^^ flatmap
                .collect(Collectors.toSet());

// g) uzyskaj listę nazwisk programistów, którzy znają więcej, niż dwa języki
        List<String> kumaciProgramisci = programmers.stream()
                .filter(programmer -> programmer.getLanguages().size() > 2)
                .map(programmer -> programmer.getPerson().getLastName())
                .collect(Collectors.toList());

// h) sprawdź, czy istnieje chociaż jedna osoba, która nie zna żadnego języka
        long ilosc = programmers.stream()
                .filter(programmer -> programmer.getLanguages().size() == 0)
                .count();

// i)* uzyskaj ilość wszystkich języków opanowanych przez programistki
        // distinct
        long iloscJezykow = programmers.stream()
                .filter(programmer -> !programmer.getPerson().isMale())
                // List<List<String>>
                .map(programmer -> programmer.getLanguages())
                .flatMap(strings -> strings.stream()) // 'spłaszczamy' listę list
                .distinct() //tylko unikalne
                .count(); // zliczamy ilość elementów

        int ilosc2 = programmers.stream()
                .filter(programmer -> !programmer.getPerson().isMale())
                // List<List<String>>
                .map(programmer -> programmer.getLanguages())
                .flatMapToInt(strings -> {
                    return Arrays.asList(strings.size()).stream().mapToInt(value -> value);
                }) // 'spłaszczamy' listę list do listy ilości języków w każdej liście
                .sum(); // zliczamy ilość elementów

    }
}
