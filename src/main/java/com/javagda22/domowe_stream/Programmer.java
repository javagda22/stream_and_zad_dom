package com.javagda22.domowe_stream;

import java.util.List;

public class Programmer{
    private Person person;
    private List<String> languages;

    public Programmer() {
    }

    public Programmer(Person p, List<String> langs){
        this.person = p;
        this.languages = langs;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public List<String> getLanguages() {
        return languages;
    }

    public void setLanguages(List<String> languages) {
        this.languages = languages;
    }

    @Override
    public String toString() {
        return "Programmer{" +
                "person=" + person +
                ", languages=" + languages +
                '}';
    }
}
