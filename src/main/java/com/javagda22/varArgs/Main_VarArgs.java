package com.javagda22.varArgs;

import java.util.Arrays;
import java.util.HashSet;

public class Main_VarArgs {

    public static void metodaDodajacaTeksty(String... arg) {
        System.out.println("Ilość:" + arg.length);
        System.out.println("Element 0:" + arg[0]);

        //... dodać do siebie wszystkie string'i i wypisać wynik na ekran
        // 1. zadeklaruj zmienną typu String
        String wynik = "";

        // 2. w pętli do zmiennej 'dodaj' wszystkie 'teksty' z tablicy arg
        for (int i = 0; i < arg.length; i++) {
            wynik += arg[i];
        }

        // 3. wypisz wynik na ekran
        System.out.println(wynik);
    }

    // 1. Do  zrobienia zadania a, b, c, LUB ZAMIAST NICH d
    // a) Napisz metodę, która przyjmuje dowolną liczbę liczb typu Double
    // (varargs) i tworzy zbiór (HashSet), składający się z tych elementów
    public static HashSet<Double> umiescWSecie(Double... liczby){
        HashSet<Double> zbior = new HashSet<>();
        // ... dodanie

        for (int i = 0; i < liczby.length; i++) {
            // każdą liczbę kolejno dodajemy do zbioru
            zbior.add(liczby[i]);
        }

        // zwracamy zbiór
        return zbior;
    }

    // b) Napisz metodę, która przyjmuje dowolną liczbę liczb typu String
    // (varargs) i tworzy zbiór (HashSet), składający się z tych elementów
    public static HashSet<String> umiescWSecie(String... liczby){
        HashSet<String> zbior = new HashSet<>();
        for (int i = 0; i < liczby.length; i++) {
            zbior.add(liczby[i]);
        }
        return zbior;
    }

    // c) Napisz metodę, która przyjmuje dowolną liczbę liczb typu Integer
    // (varargs) i tworzy zbiór (HashSet), składający się z tych elementów
    public static HashSet<Integer> umiescWSecie(Integer... liczby){
        HashSet<Integer> zbior = new HashSet<>();
        for (int i = 0; i < liczby.length; i++) {
            zbior.add(liczby[i]);
        }
        return zbior;
    }

    public static <T> HashSet<T> umiescWSecie(T... liczby){
        HashSet<T> zbior = new HashSet<>();
        for (int i = 0; i < liczby.length; i++) {
            zbior.add(liczby[i]);
        }
        return zbior;
    }

    public static void main(String[] args) {
        metodaDodajacaTeksty("j", "a", "v", "a", "2", "2");

        System.out.println(umiescWSecie(2.3, 3.5, 5.6, 18.0));;
    }

    // d*) Napisz metodę generyczną <T>, która przyjmuje dowolną liczbę
    // argumentów dowolnego typu
    // (varargs) i tworzy zbiór (HashSet), składający się z tych elementów
    // WSKAZÓWKA: public static <T> Set<T> newSet(T... data)

    // klawisz Insert!
    // uruchamia tryb zastępowania, zamiast pisania
}
